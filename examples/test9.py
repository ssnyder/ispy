#!/usr/bin/env tdaq_python

from __future__ import print_function

import sys

#import ers

from ipc import IPCPartition
from ispy import *

p = IPCPartition(len(sys.argv) > 1 and sys.argv[1] or 'rhauser_l2eb')
if not p.isValid():
    print("No such partition:",p.name())
    sys.exit(1)

x = ISInfoDynAny()
d = ISInfoDictionary(p)

d.getValue('Histogramming.L2SV-1./DEBUG/L2SVHistograms/TotalL2PULatencyAcceptedEvent', x)
#d.getValue('RunCtrl.RootController', x)
print(x)

y = ISInfoDynAny(p, 'DFM')

h = IS.HistogramDataInt(p, 'DF.test')
h.axes.resize(len(x.axes))
for i,hi in enumerate(h.axes):
   h.axes[i].set(x.axes[i])

print(h)

h.checkin()

dqm = IS.Result(p, 'DQM.LVL2')
dqm.status = 2
dqm['tags'].resize(1) 
dqm['tags'][0].set(name='Test', value=4.2)
print(dqm['tags'][0])
dqm.checkin()

dfm = ISObject(p, 'DF.DFM-1')
dfm.checkout()
print(dfm)

l2sv = IS.L2SV(p, 'DF.NewL2SV')
print(l2sv)
l2sv.Identity = 'fake'
l2sv.checkin()

s = ISInfoStream(p, 'DF', ISCriteria('L2PU-.*'), False, 1)
print("ISInfoStream")
while not s.eof():
       print(s.name(),s.type().name())
       val = ISInfoDynAny()
       s.get(val)
       print(val)

r = ISInfoReceiver(p)

def handler(cb):
    print(cb.name(), cb.reason(), cb.time())
    # v = ISInfoDynAny()
    # cb.value(v)
    # print v

def handler1(obj, name):
    print(name)
    print(obj)

r.subscribe_event('DF', handler, ISCriteria('DFM-1'))
r.subscribe_event('DF.DFM-1', handler)

r.subscribe_info('DF', handler, ISCriteria('DFM-[0-9]+$'))
r.subscribe_info('DF.L2SV-1', handler)

# r.subscribe('DF',handler1, ISCriteria('L2PU-8191'))

print("Type Ctrl-C to interrupt")
try:
    import time
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    r.unsubscribe('DF.DFM-1', True)
    r.unsubscribe('DF.L2SV-1', True)
    # r.unsubscribe('DF', ISCriteria('L2PU-8191'), True)
    r.unsubscribe('DF', ISCriteria('DFM-1'), True)
    r.unsubscribe('DF', ISCriteria('DFM-[0-9]+$'), True)    
    

