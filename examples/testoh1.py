#!/usr/bin/env tdaq_python

import sys

from ispy import IPCPartition
from oh import *

p = IPCPartition(sys.argv[1])

x = getRootObject(p, 'Histogramming-L2-Segment-1-1-iss', 'L2PU-8191', '/DEBUG/LVL2PUHistograms/DataPerAcceptedEvent')
x.Draw()

