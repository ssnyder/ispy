#!/usr/bin/env tdaq_python

# Configuration

from __future__ import print_function

part_name = 'rhauser_test'

# End Configuration

import sys
import time
from ispy import *

p = IPCPartition(part_name)

if not p.isValid():
    print("Partition",part_name,"is not valid")
    sys.exit(1)


####################################################

it = ISInfoIterator(p, 'DF', ISCriteria('L2SV-.*'))

while it.next():
    any = ISInfoAny()    
    it.value(any)
    any._update(it.name(), p)
    print(any)

####################################################

