#!/usr/bin/env tdaq_python

# Configuration

from __future__ import print_function

part_name = 'rhauser_test'

# End Configuration

import sys
if len(sys.argv) > 1:
   part_name = sys.argv[1]

from ispy import *

# To get ROOT in TDAQ release
import os
sys.path.append(os.environ['ROOTSYS'] + '/lib')
import ROOT

p = IPCPartition(part_name)

if not p.isValid():
    print("Partition",part_name,"is not valid")
    sys.exit(1)


####################################################

f     = ROOT.TFile.Open('histo.root','RECREATE')
hist  = ROOT.TH1F('test','test', 1000, 0.0, 30000.0)
graph = ROOT.TGraph()

base_time  = -1
count = 0

def l2sv_callback(any, name):
    global rcv
    global count
    global base_time
    global hist
    global graph

    print("hello")

    count += 1
    print(count)
    if count == 20:
        rcv.stop()

    # Convert to python type
    any._update(name, p)

    print(any)
    print(hist)
    print(graph)

    hist.Fill(any.IntervalEventRate)
       
    if base_time == -1:
       base_time = any.time().c_time()
    
    graph.SetPoint(count, any.time().c_time() - base_time, any.IntervalEventRate)


rcv = ISInfoReceiver(p)
rcv.subscribe('DF', l2sv_callback, ISCriteria('L2SV-[0-9]+'))

rcv.run()

print("Done")

graph.Write("IntervalRate")
f.Write()
f.Close()
