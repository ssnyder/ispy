
#include "ipc/exceptions.h"
#include "ipc/core.h"
#include "ipc/partition.h"
#include "ipc/applicationcontext.h"
#include "owl/time.h"

#include <boost/python.hpp>

#include <string>
#include <chrono>

using namespace boost::python;

namespace {

    void init_ipc(list py_options)
    {
        if(!IPCCore::isInitialised()) {
            std::list<std::pair<std::string, std::string>>  options;
            for(ssize_t i = 0; i < len(py_options); i++) {
                object o = py_options[i];
                std::string key = extract<std::string>(o[0]);
                std::string value = extract<std::string>(o[1]);
                options.emplace_back(key, value);
            }
            IPCCore::init(options);
        }
    }

    struct IPCInitializer {
        IPCInitializer()
        {
            setenv("TDAQ_IPC_CLIENT_INTERCEPTORS", "", 0);
            if(!IPCCore::isInitialised() && getenv("TDAQ_ISPY_NOINIT") == nullptr) {
                std::list<std::pair<std::string, std::string>>  options;
                IPCCore::init(options);
            }
#if PY_MINOR_VERSION < 3 || PY_MINOR_VERSION < 7
            PyEval_InitThreads();
#endif
        }
    };

    list get_partitions()
    {
        std::list<IPCPartition> output;
        IPCPartition::getPartitions(output);
        list result;
        for(std::list<IPCPartition>::iterator it = output.begin();
            it != output.end();
            ++it) {
            result.append(*it);
        }
        return result;
    }

    list get_types(IPCPartition& partition)
    {
        std::list<std::string> types;
        partition.getTypes(types);
        list result;
        for(auto& type : types) { result.append(type); }
        return result;
    }


    dict get_objects(IPCPartition& partition,  const std::string& type_name)
    {
        std::map<std::string, typename ipc::servant::_var_type> map;
        partition.getObjects<ipc::servant, ipc::use_cache, ipc::narrow>(map, type_name);
        dict result;
        for(auto it = map.begin(); it != map.end(); ++it) {
            IPCApplicationContext ctx(it->second);
            dict d;
            d["valid"] = ctx.m_valid;
            d["name"]  = ctx.m_name;
            d["owner"] = ctx.m_owner;
            d["host"]  = ctx.m_host;
            d["pid"]   = ctx.m_pid;
            d["time"]  = std::chrono::system_clock::to_time_t(ctx.m_time);
            d["debug"] = ctx.m_debug_level;
            d["verbose"] = ctx.m_verbosity_level;
            result[it->first] = d;
        }
        return result;
    }

    bool is_object_valid(IPCPartition& partition, const std::string& name, const std::string& type_name)
    {
        return partition.isObjectValid<ipc::servant, ipc::use_cache, ipc::narrow>(name, type_name);
    }

}

BOOST_PYTHON_MODULE(libipcpy)
{

    static IPCInitializer ipc_initialize;

    class_<OWLDate>("OWLDate")
        .def(init<const char*>())
        .def(init<time_t>())
        .def("year", &OWLDate::year)
        .def("month", &OWLDate::month)
        .def("day", &OWLDate::day)
        .def("str", &OWLDate::str)
        .def("c_time", &OWLDate::c_time)
        ;

    class_<OWLTime, bases<OWLDate> >("OWLTime")
        .def(init<const char*>())
        .def(init<time_t>())
        .def("hour", &OWLTime::hour)
        .def("min", &OWLTime::min)
        .def("sec", &OWLTime::sec)
        .def("mksec", &OWLTime::mksec)
        .def("str", &OWLTime::str)
        .def("c_time", &OWLTime::c_time)
        ;


    class_<IPCPartition>("IPCPartition")
        .def(init<std::string>())
        .def("name", &IPCPartition::name,return_value_policy<copy_const_reference>())
        .def("isValid", &IPCPartition::isValid)
        .def("getTypes", get_types)
        .def("getObjects", get_objects)
        .def("isObjectValid", is_object_valid)
        ;

    class_<IPCCore>("IPCCore")
        .def("init", &init_ipc)
        .def("isInitialised", &IPCCore::isInitialised)
        .def("shutdown", &IPCCore::shutdown)
        ;

    def("getPartitions", get_partitions);

}
